//
//  Util.swift
//  taxiApp
//
//  Created by Paolo Ramos Mendez on 12/24/18.
//  Copyright © 2018 paolo. All rights reserved.
//

import UIKit

extension String {
    
    enum RegularExpressions: String {
        case phone = "^\\s*(?:\\+?(\\d{1,3}))?([-. (]*(\\d{3})[-. )]*)?((\\d{3})[-. ]*(\\d{2,4})(?:[-.x ]*(\\d+))?)\\s*$"
    }
    
    func isValid(regex: RegularExpressions) -> Bool {
        return isValid(regex: regex.rawValue)
    }
    
    func isValid(regex: String) -> Bool {
        let matches = range(of: regex, options: .regularExpression)
        return matches != nil
    }
    
    func onlyDigits() -> String {
        let filtredUnicodeScalars = unicodeScalars.filter{CharacterSet.decimalDigits.contains($0)}
        return String(String.UnicodeScalarView(filtredUnicodeScalars))
    }
    
    func makeACall() {
        if isValid(regex: .phone) {
            if let url = URL(string: "tel://\(self.onlyDigits())"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
            else {
                Util.showAlertView(message: "Este dispositivo no puede hacer una llamada.")
            }
        }
    }
}

public extension UIAlertController {
    func show() {
        let win = UIWindow(frame: UIScreen.main.bounds)
        let vc = UIViewController()
        vc.view.backgroundColor = .clear
        win.rootViewController = vc
        win.windowLevel = UIWindow.Level.alert + 1
        win.makeKeyAndVisible()
        vc.present(self, animated: true, completion: nil)
    }
}

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}

class Util: NSObject {
    class func getContainerFrame(_ viewController: UIViewController, notificationHeight: CGFloat = CGFloat(0.0)) -> CGRect {
        if let frame = viewController.navigationController?.navigationBar.frame {
            var y = frame.origin.y
            if y == CGFloat(0.0) {
                y = frame.size.height
            }
            return CGRect(x: viewController.view.frame.origin.x,
                          y: y + frame.size.height + notificationHeight,
                          width: viewController.view.frame.width,
                          height: viewController.view.frame.height - frame.size.height - notificationHeight - y)
        }
        return CGRect.zero
    }
    
    class func getSizeiPhone() -> Bool {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                //                print("iPhone 5 or 5S or 5C")
                return false
            case 1334:
                //                print("iPhone 6/6S/7/8")
                return false
            case 2208:
                //                print("iPhone 6+/6S+/7+/8+")
                return false
            case 2436:
                //                print("iPhone X")
                return true
            default:
                //                print("unknown")
                return false
            }
        }
        return false
    }
    
    class func showAlertView(vc: UIViewController? = nil, title: String? = nil, message: String? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        if vc != nil {
            vc!.present(alert, animated: true, completion: nil)
        }
        else {
            alert.show()
        }
    }
    
    class func configureTitleView(_ viewController: UIViewController ,nameXib: String) {
        let navTitleView : UIView = Bundle.main.loadNibNamed(nameXib, owner: nil, options: nil)![0] as! UIView
        viewController.navigationItem.titleView = navTitleView
    }
}
