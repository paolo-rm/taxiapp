//
//  SettingsView.swift
//  taxiApp
//
//  Created by Paolo Ramos Mendez on 1/10/19.
//  Copyright © 2019 paolo. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class SettingsView: UIView {
    
    let nibName = "SettingsView"
    // Our custom view from the XIB file
    @IBOutlet var view: UIView!
    @IBOutlet weak var tableView: UITableView!
    var mailArray = [String]()
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.xibSetup()
    }
    
    func xibSetup() {
        self.view = loadViewFromNib()
        self.view.frame = bounds
        self.view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        self.addSubview(self.view)
        self.configureView()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func configureView() {
        self.tableView.register(UINib(nibName: "MailTableViewCell", bundle: nil), forCellReuseIdentifier: "MailTableViewCell")
        self.tableView.register(UINib(nibName: "HeaderTableView", bundle: nil), forHeaderFooterViewReuseIdentifier: "HeaderTableView")
        self.tableView.register(UINib(nibName: "AddTableViewCell", bundle: nil), forCellReuseIdentifier: "AddTableViewCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        if let array = Preferences.getMailArray() {
            self.mailArray = array
        }
    }
    
    func validateMailArray() {
        if self.mailArray.count >= 1 {
            if !self.mailArray[0].isEmpty {
                Preferences.setMailArray(self.mailArray)
            }
        }
    }
    
    func isValidTextField(_ textField: SkyFloatingLabelTextField) -> Bool {
        if textField.text != nil {
            if textField.errorMessage == nil  {
                return !textField.text!.isEmpty
            }
            return false
        }
        return false
    }
    
    @objc func textFieldDidChange(_ textfield: SkyFloatingLabelTextField) {
        if let text = textfield.text {
            let value = self.isValidEmail(testStr: text)
            if value {
                self.mailArray[textfield.tag] = text
                textfield.errorMessage = nil
            }
            else {
                self.mailArray[textfield.tag] = ""
                textfield.errorMessage = "Correo invalido"
            }
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}

extension SettingsView: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderTableView") as! HeaderTableView
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(40.0)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(50.0)
    }
}

extension SettingsView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mailArray.count+1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row) == self.mailArray.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddTableViewCell") as! AddTableViewCell
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "MailTableViewCell") as! MailTableViewCell
        cell.textField.tag = indexPath.row
        cell.textField.text = self.mailArray[indexPath.row]
        cell.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == self.mailArray.count {
            if self.mailArray.count < 5 {
                var index = indexPath
                index.row = indexPath.row - 1
                if let cell = self.tableView.cellForRow(at: index) as? MailTableViewCell {
                    if self.isValidTextField(cell.textField) {
                        self.mailArray.append("")
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        if indexPath.row != self.mailArray.count && self.mailArray.count > 1 {
            let delete = UITableViewRowAction(style: .destructive, title: "Borrar") { (action, indexPath) in
                self.mailArray.remove(at: indexPath.row)
                self.tableView.deleteRows(at: [indexPath], with: .fade)
                self.tableView.reloadData()
            }
            return [delete]
        }
        return []
    }
}
