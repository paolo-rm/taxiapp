//
//  MailTableViewCell.swift
//  taxiApp
//
//  Created by Paolo Ramos Mendez on 1/10/19.
//  Copyright © 2019 paolo. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class MailTableViewCell: UITableViewCell {

    @IBOutlet weak var textField: SkyFloatingLabelTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.textField.placeholder = "Correo"
        self.textField.title = "Correo valido"
        self.textField.errorColor = UIColor.red
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    
}
