//
//  SettingsViewController.swift
//  taxiApp
//
//  Created by Paolo Ramos Mendez on 1/7/19.
//  Copyright © 2019 paolo. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    var settingsView: SettingsView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Util.configureTitleView(self, nameXib: "ConfigurationTitle")
        self.settingsView = SettingsView(frame: Util.getContainerFrame(self))
        self.view.addSubview(self.settingsView)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated);
        if self.isMovingFromParent
        {
            //On click of back or swipe back
            print("back")
            self.settingsView.validateMailArray()
            
        }
        if self.isBeingDismissed
        {
            //Dismissed
            print("dissmiss")
        }
    }

}
