//
//  RaceViewController.swift
//  taxiApp
//
//  Created by Paolo Ramos Mendez on 1/7/19.
//  Copyright © 2019 paolo. All rights reserved.
//

import UIKit

class RaceViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        Util.configureTitleView(self, nameXib: "RaceTitle")

    }
}
