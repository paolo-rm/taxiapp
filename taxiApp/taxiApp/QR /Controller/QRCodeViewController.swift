//
//  QRCodeViewController.swift
//  taxiApp
//
//  Created by Paolo Ramos Mendez on 1/7/19.
//  Copyright © 2019 paolo. All rights reserved.
//

import AVFoundation
import UIKit
import QRCodeReader
import Alamofire
import SwiftyJSON
import JGProgressHUD

class QRCodeViewController: UIViewController, QRCodeReaderViewControllerDelegate {
    
    lazy var reader: QRCodeReader = QRCodeReader()
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader                  = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
            $0.showTorchButton         = true
            $0.preferredStatusBarStyle = .lightContent
            
            $0.reader.stopScanningWhenCodeIsFound = false
        }
        return QRCodeReaderViewController(builder: builder)
    }()
    
    var qrCodeView: QRCodeView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Util.configureTitleView(self, nameXib: "QRCodeTitle")
        self.scanInModalAction()
        self.qrCodeView = QRCodeView(frame: Util.getContainerFrame(self))
        self.view.addSubview(self.qrCodeView)
        self.qrCodeView.isHidden = true
    }
    
    func sendReport(code: String) {
        let codeToSend = code.trimmingCharacters(in: .whitespacesAndNewlines)
        if codeToSend.isEmpty {
            let alert = UIAlertController(title: "", message: "Introduzca un código", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { _ in
                self.returnMenu()
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Enviando"
        hud.show(in: self.view)
        let headers: HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: Parameters = [
            "email": "\(Preferences.getMailString())",
            "report": "1b",
            "id": "\(Preferences.getLastTaxiId())",
            "userId": "\(Preferences.getName())",
            "code": "\(codeToSend)"
        ]
        AF.request("http://69.164.197.243:58081/inmoviliza/endPointAlfa.php", method: .post, parameters: parameters, headers: headers).responseJSON {
            response in
            hud.dismiss(animated: true)
            if let json = response.result.value {
                let jsonData = JSON(json)
//                print("JSON DATA >>>>> \(jsonData)")
                if jsonData["codeMatch"].exists() {
                    self.qrCodeView.checkReport(value: false)
                    return
                }
                if jsonData["reporte"].stringValue == "error" {
                    self.qrCodeView.checkReport(value: false)
                    return
                }
                if jsonData["reporte"].stringValue == "OK" {
                    self.qrCodeView.checkReport(value: true)
                    let alert = UIAlertController(title: "Reporte Exitoso.", message: "Los datos referentes a esta carrera individual serán enviados a tus correos referidos.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return
                }
            }
        }
    }
    
    func getQRInfoService(taxiCode: String) {
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Cargando"
        hud.show(in: self.view)
        let parameters: Parameters = ["action": "driverInfo", "taxiId": taxiCode]
        AF.request("http://72.14.187.88/PT_API_alfa/User.php", parameters: parameters).responseJSON {
            response in
            hud.dismiss(animated: true)
            if let json = response.result.value {
                let jsonData = JSON(json)
                if jsonData["Error"].exists() {
                    let alert = UIAlertController(title: "Error", message: "Taxi no registrado", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { _ in
                        self.returnMenu()
                    }))
                    self.present(alert, animated: true, completion: nil)
                    return
                }
                
                let driver = DriverModel(name: jsonData["conductor"].stringValue,
                                         ci: jsonData["ci"].stringValue,
                                         cellPhone: jsonData["tel"].stringValue,
                                         driverImgUrl: jsonData["urlConductorImg"].stringValue)
                
                let car = CarModel(brand: jsonData["marca"].stringValue,
                                   model: jsonData["modelo"].stringValue,
                                   year: jsonData["anho"].stringValue,
                                   color: jsonData["color"].stringValue,
                                   plate: jsonData["placa"].stringValue,
                                   company: jsonData["empresa"].stringValue,
                                   city: jsonData["cuidad"].stringValue,
                                   companyImgUrl: jsonData["urlEmpresaImg"].stringValue,
                                   carImgUrl: jsonData["urlAutoImg"].stringValue)
                self.qrCodeView.uploadData(car: car, driver: driver)
                self.qrCodeView.isHidden = false
            }
        }
    }
    
    func notReportRace() {
        self.returnMenu()
    }
    
    // MARK: - Actions
    private func checkScanPermissions() -> Bool {
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
        } catch let error as NSError {
            let alert: UIAlertController
            
            switch error.code {
            case -11852:
                alert = UIAlertController(title: "Error", message: "Esta aplicación no está autorizada para utilizar la cámara.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                            if let url = URL(string: "\(settingsURL)") {
                                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                            }
                        }
                    }
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
                    self.returnMenu()
                }))
                
            default:
                alert = UIAlertController(title: "Error", message: "Lector no compatible con el dispositivo actual.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { _ in
                    self.returnMenu()
                }))
            }
            
            present(alert, animated: true, completion: nil)
            
            return false
        }
    }
    
    func returnMenu() {
        if let nc = self.navigationController {
            for vc in nc.viewControllers {
                if vc.isKind(of: MenuViewController.self) {
                    self.navigationController?.popToViewController(vc, animated: true)
                    break
                }
            }
        }
    }
    
    func scanInModalAction() {
        guard checkScanPermissions() else { return }
        
        readerVC.modalPresentationStyle = .formSheet
        readerVC.delegate               = self
        
        readerVC.completionBlock = { (result: QRCodeReaderResult?) in
            if let result = result {
                print("Completion with result: \(result.value) of type \(result.metadataType)")
                Preferences.setLastTaxiId(result.value)
                self.getQRInfoService(taxiCode: result.value)
            }
        }
        present(readerVC, animated: true, completion: nil)
    }
    
    // MARK: - QRCodeReader Delegate Methods
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        reader.dismiss(animated: true) {
            self.returnMenu()
        }
    }
    
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
        print("Switching capturing to: \(newCaptureDevice.device.localizedName)")
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        reader.dismiss(animated: true, completion: nil)
        self.returnMenu()
    }

}
