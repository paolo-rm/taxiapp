//
//  DriverModel.swift
//  taxiApp
//
//  Created by Paolo Ramos Mendez on 1/13/19.
//  Copyright © 2019 paolo. All rights reserved.
//

import UIKit

struct DriverModel {
    let name: String
    let ci: String
    let cellPhone: String
    let driverImgUrl: String
}
