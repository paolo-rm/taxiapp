//
//  CarModel.swift
//  taxiApp
//
//  Created by Paolo Ramos Mendez on 1/13/19.
//  Copyright © 2019 paolo. All rights reserved.
//

import UIKit

struct CarModel {
    let brand: String
    let model: String
    let year: String
    let color: String
    let plate: String
    let company: String
    let city: String
    let companyImgUrl: String
    let carImgUrl: String
}

