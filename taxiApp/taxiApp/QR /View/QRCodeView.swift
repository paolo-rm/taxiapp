//
//  QRCodeView.swift
//  taxiApp
//
//  Created by Paolo Ramos Mendez on 1/10/19.
//  Copyright © 2019 paolo. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class QRCodeView: UIView, UITextFieldDelegate {

    let nibName = "QRCodeView"
    // Our custom view from the XIB file
    @IBOutlet var view: UIView!
    @IBOutlet weak var reportRaceBtn: UIButton!
    @IBOutlet weak var noReportRaceBtn: UIButton!
    
    @IBOutlet weak var companyImg: UIImageView!
    @IBOutlet weak var driverImg: UIImageView!
    @IBOutlet weak var taxiImg: UIImageView!
    
    @IBOutlet weak var driverInformation: UILabel!
    @IBOutlet weak var carInformation: UILabel!
    
    @IBOutlet weak var codeTF: SkyFloatingLabelTextField!
    
    var car = CarModel(brand: "",
                       model: "",
                       year: "",
                       color: "",
                       plate: "",
                       company: "",
                       city: "",
                       companyImgUrl: "",
                       carImgUrl: "")
    
    var driver = DriverModel(name: "",
                             ci: "",
                             cellPhone: "",
                             driverImgUrl: "")
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.xibSetup()
    }
    
    func xibSetup() {
        self.view = loadViewFromNib()
        self.view.frame = bounds
        self.view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        self.addSubview(self.view)
        self.configureView()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func configureView() {
        self.reportRaceBtn.titleLabel?.lineBreakMode = .byWordWrapping
        self.reportRaceBtn.titleLabel?.numberOfLines = 2
        self.noReportRaceBtn.titleLabel?.lineBreakMode = .byWordWrapping
        self.noReportRaceBtn.titleLabel?.numberOfLines = 2
        
        self.codeTF.placeholder = "Introduzca codigo"
        self.codeTF.title = "Codigo"
        self.codeTF.errorColor = UIColor.red
        self.codeTF.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let vc = self.parentViewController as? QRCodeViewController {
            vc.sendReport(code: textField.text ?? "")
        }
        self.view.endEditing(true)
        return false
    }
    
    func checkReport(value: Bool) {
        if value {
            self.codeTF.title = "Codigo enviado"
        }
        self.codeTF.errorMessage = "Codigo Invalido"
    }
    
    func uploadData(car: CarModel, driver: DriverModel) {
        self.companyImg.downloaded(from: car.companyImgUrl)
        self.taxiImg.downloaded(from: car.carImgUrl)
        self.driverImg.downloaded(from: driver.driverImgUrl)
        
        let driverInfo = "Nombre: \(driver.name)\n" +
                         "C.I.: \(driver.ci)\n" +
                         "Número de Telefono: \(driver.cellPhone)\n"
        self.driverInformation.text = driverInfo
        
        let carInfo = "Marca: \(car.brand)\n" +
            "Modelo: \(car.model)\n" +
            "Año: \(car.year)\n" +
            "Color: \(car.color)\n" +
            "Placa: \(car.plate)\n" +
            "Empresa: \(car.company)\n" +
            "Ciudad: \(car.city)\n"
        self.carInformation.text = carInfo
    }

    @IBAction func reportRace(_ sender: Any) {
        if let vc = self.parentViewController as? QRCodeViewController {
            vc.sendReport(code: self.codeTF.text ?? "")
        }
    }
    
    @IBAction func notReportRace(_ sender: Any) {
        if let vc = self.parentViewController as? QRCodeViewController {
            vc.notReportRace()
        }
    }
    
}
