//
//  Preferences.swift
//  taxiApp
//
//  Created by Paolo Ramos Mendez on 1/15/19.
//  Copyright © 2019 paolo. All rights reserved.
//

import UIKit

class Preferences: NSObject {
    
    class func getFirstTime() -> Bool {
        let defaults : UserDefaults = UserDefaults.standard
        return defaults.object(forKey: "first_time") as? Bool ?? Bool()
    }
    
    class func setFirstTime(_ bool: Bool) {
        let defaults : UserDefaults = UserDefaults.standard
        defaults.set(bool, forKey: "first_time")
    }
    
    class func setName(_ value : String)  {
        let defaults : UserDefaults = UserDefaults.standard
        defaults.setValue(value, forKey: "user_name")
    }
    
    class func getName() -> String  {
        let defaults : UserDefaults = UserDefaults.standard
        let resultMode = defaults.string(forKey: "user_name")
        if resultMode != nil {
            return resultMode!
        }
        return ""
    }
    
    class func setPhone(_ value : String)  {
        let defaults : UserDefaults = UserDefaults.standard
        defaults.setValue(value, forKey: "user_phone")
    }
    
    class func getPhone() -> String  {
        let defaults : UserDefaults = UserDefaults.standard
        let resultMode = defaults.string(forKey: "user_phone")
        if resultMode != nil {
            return resultMode!
        }
        return ""
    }

    class func setMail(_ value : String)  {
        let defaults : UserDefaults = UserDefaults.standard
        defaults.setValue(value, forKey: "user_mail")
    }
    
    class func getMail() -> String  {
        let defaults : UserDefaults = UserDefaults.standard
        let resultMode = defaults.string(forKey: "user_mail")
        if resultMode != nil {
            return resultMode!
        }
        return ""
    }
    
    class func setLastTaxiId(_ value : String)  {
        let defaults : UserDefaults = UserDefaults.standard
        defaults.setValue(value, forKey: "taxi_id")
    }
    
    class func getLastTaxiId() -> String  {
        let defaults : UserDefaults = UserDefaults.standard
        let resultMode = defaults.string(forKey: "taxi_id")
        if resultMode != nil {
            return resultMode!
        }
        return ""
    }
    
    class func setMailArray(_ value : [String])  {
        let defaults : UserDefaults = UserDefaults.standard
        defaults.setValue(value, forKey: "mail_array")
    }
    
    class func getMailArray() -> [String]?  {
        let defaults : UserDefaults = UserDefaults.standard
        if let resultMode = defaults.array(forKey: "mail_array") as? [String] {
            return resultMode
        }
        return nil
    }
    
    class func getMailString() -> String {
        let defaults : UserDefaults = UserDefaults.standard
        if let resultMode = defaults.array(forKey: "mail_array") as? [String] {
            var mailString = ""
            resultMode.forEach { (mail) in
                if !mail.isEmpty {
                    mailString.append("\(mail),")
                }
            }
            mailString.removeLast()
            return mailString
        }
        return ""
    }
    
    class var FILTER_DEFAULT: String { return "Todas las empresas" }
}
