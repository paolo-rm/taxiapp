//
//  RegisterViewController.swift
//  taxiApp
//
//  Created by Paolo Ramos Mendez on 12/24/18.
//  Copyright © 2018 paolo. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    var registerView: RegisterView!

    override func viewDidLoad() {
        super.viewDidLoad()
        Util.configureTitleView(self, nameXib: "MainTitle")
        self.registerView = RegisterView(frame: Util.getContainerFrame(self))
        self.view.addSubview(self.registerView)
    }
}
