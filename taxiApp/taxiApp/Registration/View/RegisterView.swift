//
//  RegisterView.swift
//  taxiApp
//
//  Created by Paolo Ramos Mendez on 12/24/18.
//  Copyright © 2018 paolo. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class RegisterView: UIView {

    let nibName = "RegisterView"
    // Our custom view from the XIB file
    @IBOutlet var view: UIView!
    
    @IBOutlet weak var fullNameTF: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneTF: SkyFloatingLabelTextField!
    @IBOutlet weak var mailTF: SkyFloatingLabelTextField!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.xibSetup()
    }
    
    func xibSetup() {
        self.view = loadViewFromNib()
        self.view.frame = bounds
        self.view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        self.addSubview(self.view)
        self.configureView()
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func configureView() {
        
        self.fullNameTF.placeholder = "Nombre completo"
        self.fullNameTF.title = "Nombre completo"
        self.fullNameTF.errorColor = UIColor.red
        self.fullNameTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.fullNameTF.tag = 1
        
        self.phoneTF.placeholder = "Número de teléfono +591XXXXXXX"
        self.phoneTF.title = "Teléfono válido"
        self.phoneTF.errorColor = UIColor.red
        self.phoneTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.phoneTF.tag = 2
        
        self.mailTF.placeholder = "Correo"
        self.mailTF.title = "Correo válido"
        self.mailTF.errorColor = UIColor.red
        self.mailTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.mailTF.tag = 3
        
    }
    
    @IBAction func registerAction(_ sender: UIButton) {
        if self.isValidTextField(self.phoneTF) && self.isValidTextField(self.fullNameTF) && self.isValidTextField(self.mailTF) {
            Preferences.setName(self.fullNameTF.text ?? "")
            Preferences.setMail(self.mailTF.text ?? "")
            Preferences.setPhone(self.phoneTF.text ?? "")
            Preferences.setMailArray([Preferences.getMail()])
            Preferences.setFirstTime(true)
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "MenuNavController") as! UINavigationController
            self.parentViewController!.present(vc, animated: true, completion: nil)
        }
        else {
            Util.showAlertView(vc: self.parentViewController!, message: "Necesitas llenar el registro")
            Preferences.setFirstTime(false)
        }
    }
    
    @objc func textFieldDidChange(_ textfield: UITextField) {
        if let text = textfield.text {
            if let floatingLabelTextField = textfield as? SkyFloatingLabelTextField {
                switch floatingLabelTextField.tag {
                case 1:
                    let fullName = text.trimmingCharacters(in: .whitespacesAndNewlines)
                    self.fullNameTF.errorMessage = fullName.isEmpty ? "Introduzca su nombre completo." : nil
                case 2:
                    self.phoneTF.errorMessage = self.isValidPhoneNumber(value: text) ? nil : "Teléfono inválido"
                case 3:
                    self.mailTF.errorMessage = self.isValidEmail(testStr: text) ? nil : "Correo inválido"
                default:
                    print("Otherwise, do something else.")
                }
            }
        }
    }
    
    func isValidTextField(_ textField: SkyFloatingLabelTextField) -> Bool {
        if textField.text != nil {
            if textField.errorMessage == nil  {
                return !textField.text!.isEmpty
            }
            return false
        }
        return false
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func isValidPhoneNumber(value: String) -> Bool {
        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
}
