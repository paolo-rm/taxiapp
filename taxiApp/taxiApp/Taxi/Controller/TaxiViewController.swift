//
//  TaxiViewController.swift
//  taxiApp
//
//  Created by Paolo Ramos Mendez on 1/7/19.
//  Copyright © 2019 paolo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JGProgressHUD

class TaxiViewController: UIViewController {

    var taxiListView: TaxiListView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Util.configureTitleView(self, nameXib: "TaxiListTitle")
        self.taxiListView = TaxiListView(frame: Util.getContainerFrame(self))
        self.view.addSubview(self.taxiListView)
        self.getTaxiList(filter: Preferences.FILTER_DEFAULT)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.topItem?.title = "Empresa de Taxis"
    }
    
    func getTaxiList(filter: String) {
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Cargando"
        hud.show(in: self.view)
        let parameters: Parameters = ["action": "listEmpresas"]
        AF.request("http://72.14.187.88/PT_API_alfa/User.php", parameters: parameters).responseJSON {
            response in
            if let json = response.result.value {
                self.uploadTaxiList(dataJson: JSON(json), filter: filter)
            }
            hud.dismiss()
        }
    }

    func uploadTaxiList(dataJson: JSON, filter: String) {
        let dataArray = dataJson.arrayValue
        var taxiArray = [TaxiModel]()
        dataArray.forEach { (item) in
            if item["ciudad"].stringValue == filter || filter == Preferences.FILTER_DEFAULT {
                taxiArray.append(TaxiModel(name: item["nombre"].stringValue,
                                           phone: item["tel"].stringValue,
                                           cellPhone: item["cel"].stringValue,
                                           urlImg: item["urlLogo"].stringValue,
                                           city: item["ciudad"].stringValue))
            }
        }
        self.taxiListView.uploadData(data: taxiArray)
    }
    
    func goTaxiDetailView(taxiData: TaxiModel) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "TaxiDetailViewController") as! TaxiDetailViewController
        vc.taxiData = taxiData
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
