//
//  TaxiDetailViewController.swift
//  taxiApp
//
//  Created by Paolo Ramos Mendez on 1/9/19.
//  Copyright © 2019 paolo. All rights reserved.
//

import UIKit

class TaxiDetailViewController: UIViewController {

    var taxiDetailView: TaxiDetailView!
    var taxiData = TaxiModel(name: "", phone: "", cellPhone: "", urlImg: "", city: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Util.configureTitleView(self, nameXib: "TaxiTitle")
        self.taxiDetailView = TaxiDetailView(frame: Util.getContainerFrame(self))
        let controlWhatsApp = UIApplication.shared.canOpenURL(URL(string: "whatsapp://test")!)
        self.taxiDetailView.updateTaxiData(taxiData: self.taxiData, whatsapp: controlWhatsApp)
        self.view.addSubview(self.taxiDetailView)
    }
    
    func callAction(taxiData: TaxiModel) {
        taxiData.phone.makeACall()
    }
    
    func whatsAppAction(taxiData: TaxiModel) {
        self.sendMessage(taxiData: taxiData)
    }
    
    func checkRateAction(taxiData: TaxiModel) {
        self.sendMessage(taxiData: taxiData)
    }
    
    func sendMessage(taxiData: TaxiModel) {
        let urlWhats = "whatsapp://send?phone=+591\(taxiData.cellPhone)&abid=+591\(taxiData.cellPhone)&text=Deseo consultar su tarifa"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL) {
                    if let url = URL(string: "\(whatsappURL)") {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                }
                else {
                    print("Install Whatsapp")
                }
            }
        }
    }
    
    
}
