//
//  TaxiModel.swift
//  taxiApp
//
//  Created by Paolo Ramos Mendez on 1/9/19.
//  Copyright © 2019 paolo. All rights reserved.
//

import UIKit

struct TaxiModel {
    let name: String
    let phone: String
    let cellPhone: String
    let urlImg: String
    let city: String
}
