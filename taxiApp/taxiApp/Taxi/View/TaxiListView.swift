//
//  TaxiListView.swift
//  taxiApp
//
//  Created by Paolo Ramos Mendez on 1/8/19.
//  Copyright © 2019 paolo. All rights reserved.
//

import UIKit
import Popover

class TaxiListView: UIView {

    let nibName = "TaxiListView"
    // Our custom view from the XIB file
    @IBOutlet var view: UIView!
    @IBOutlet weak var filterLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    let popover = Popover()
    var data = [TaxiModel]()
    var filterArray = [Preferences.FILTER_DEFAULT,
                       "La Paz",
                       "Cochabamba",
                       "Santa Cruz",
                       "Beni",
                       "Pando",
                       "Oruro",
                       "Potosi",
                       "Tarija",
                       "Sucre"]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.xibSetup()
    }
    
    func xibSetup() {
        self.view = loadViewFromNib()
        self.view.frame = bounds
        self.view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        self.addSubview(self.view)
        self.configureView()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func configureView() {
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(UINib.init(nibName: "TaxiCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TaxiCollectionViewCell")
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        self.filterLabel.addGestureRecognizer(tapGesture)
        self.filterLabel.isUserInteractionEnabled = true
    }
    
    func uploadData(data: [TaxiModel]) {
        self.data = data
        self.collectionView.reloadData()
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer) {
        let startPoint = CGPoint(x: CGFloat(self.view.frame.width/2), y: CGFloat(self.collectionView.frame.origin.y + 60))
        let tableView = UITableView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 180))
        tableView.dataSource = self
        tableView.delegate = self
        self.popover.show(tableView, point: startPoint)
    }
}

extension TaxiListView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filterArray.count
    }
}

extension TaxiListView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = self.filterArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.filterLabel.text = self.filterArray[indexPath.row]
        if let vc = self.parentViewController as? TaxiViewController {
            vc.getTaxiList(filter: self.filterLabel.text ?? Preferences.FILTER_DEFAULT)
        }
        self.popover.dismiss()
    }
}

extension TaxiListView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat = 10
        let collectionViewSize = collectionView.frame.size.width - padding
        return CGSize(width: (collectionViewSize/2), height: 80.0)
    }
}

extension TaxiListView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.data.count
    }
}

extension TaxiListView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TaxiCollectionViewCell", for: indexPath) as! TaxiCollectionViewCell
        cell.configure(with: self.data[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let vc = self.parentViewController as? TaxiViewController {
            vc.goTaxiDetailView(taxiData: self.data[indexPath.row])
        }
    }
}
