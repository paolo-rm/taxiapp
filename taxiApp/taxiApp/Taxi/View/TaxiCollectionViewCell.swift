//
//  TaxiCollectionViewCell.swift
//  taxiApp
//
//  Created by Paolo Ramos Mendez on 1/9/19.
//  Copyright © 2019 paolo. All rights reserved.
//

import UIKit

class TaxiCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    public func configure(with model: TaxiModel) {
        self.imgView.downloaded(from: model.urlImg)
        self.titleLbl.text = model.name
    }
    
}
