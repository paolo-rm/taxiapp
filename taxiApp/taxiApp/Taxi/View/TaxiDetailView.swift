//
//  TaxiDetailView.swift
//  taxiApp
//
//  Created by Paolo Ramos Mendez on 1/9/19.
//  Copyright © 2019 paolo. All rights reserved.
//

import UIKit

class TaxiDetailView: UIView {

    let nibName = "TaxiDetailView"
    // Our custom view from the XIB file
    @IBOutlet var view: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var whatsAppBtn: UIButton!
    @IBOutlet weak var checkRateBtn: UIButton!
    
    var taxiData = TaxiModel(name: "", phone: "", cellPhone: "", urlImg: "", city: "")
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.xibSetup()
    }
    
    func xibSetup() {
        self.view = loadViewFromNib()
        self.view.frame = bounds
        self.view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        self.addSubview(self.view)
        self.configureView()
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func configureView() {
        self.textView.isUserInteractionEnabled = false
    }
    
    func updateTaxiData(taxiData: TaxiModel, whatsapp: Bool = true) {
        self.taxiData = taxiData
        var dataString = "\n \(self.taxiData.name)"
        dataString.append("\n Telefono: \(self.taxiData.phone)")
        dataString.append("\n Celular: \(self.taxiData.cellPhone)")
        dataString.append("\n Ciudad: \(self.taxiData.city)")
        self.textView.text = dataString
        self.imgView.downloaded(from: taxiData.urlImg)
        
        self.whatsAppBtn.isHidden = !whatsapp
        self.checkRateBtn.isHidden = !whatsapp
    }
    
    @IBAction func callAction(_ sender: UIButton) {
        if let vc = self.parentViewController as? TaxiDetailViewController {
            vc.callAction(taxiData: self.taxiData)
        }
    }
    
    @IBAction func whatsAppAction(_ sender: UIButton) {
        if let vc = self.parentViewController as? TaxiDetailViewController {
            vc.whatsAppAction(taxiData: self.taxiData)
        }
    }
    
    @IBAction func checkRateAction(_ sender: UIButton) {
        if let vc = self.parentViewController as? TaxiDetailViewController {
            vc.checkRateAction(taxiData: self.taxiData)
        }
    }
}
