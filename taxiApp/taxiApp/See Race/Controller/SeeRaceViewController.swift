//
//  SeeRaceViewController.swift
//  taxiApp
//
//  Created by Paolo Ramos Mendez on 1/7/19.
//  Copyright © 2019 paolo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JGProgressHUD

class SeeRaceViewController: UIViewController {

    var seeRaceView: SeeRaceView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Util.configureTitleView(self, nameXib: "SeeRaceTitle")
        self.seeRaceView = SeeRaceView(frame: Util.getContainerFrame(self))
        self.view.addSubview(self.seeRaceView)
        self.seeRaceView.isHidden = true
        self.getInfoService(taxiCode: Preferences.getLastTaxiId())
    }
    
    func getInfoService(taxiCode: String) {
        
        if taxiCode.isEmpty {
            let alert = UIAlertController(title: nil, message: "Necesitas reportar una carrera. Puedes hacerlo en Leer Codigo QR.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { _ in
                _ = self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Cargando"
        hud.show(in: self.view)
        let parameters: Parameters = ["action": "driverInfo", "taxiId": taxiCode]
        AF.request("http://72.14.187.88/PT_API_alfa/User.php", parameters: parameters).responseJSON {
            response in
            hud.dismiss()
            if let json = response.result.value {
                let jsonData = JSON(json)
                if jsonData["Error"].exists() {
                    let alert = UIAlertController(title: "Error", message: "Taxi no registrado", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { _ in
                        _ = self.navigationController?.popViewController(animated: true)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    return
                }
                
                let driver = DriverModel(name: jsonData["conductor"].stringValue,
                                         ci: jsonData["ci"].stringValue,
                                         cellPhone: jsonData["tel"].stringValue,
                                         driverImgUrl: jsonData["urlConductorImg"].stringValue)
                
                let car = CarModel(brand: jsonData["marca"].stringValue,
                                   model: jsonData["modelo"].stringValue,
                                   year: jsonData["anho"].stringValue,
                                   color: jsonData["color"].stringValue,
                                   plate: jsonData["placa"].stringValue,
                                   company: jsonData["empresa"].stringValue,
                                   city: jsonData["cuidad"].stringValue,
                                   companyImgUrl: jsonData["urlEmpresaImg"].stringValue,
                                   carImgUrl: jsonData["urlAutoImg"].stringValue)
                self.seeRaceView.uploadData(car: car, driver: driver)
                self.seeRaceView.isHidden = false
            }
        }
    }
    
}
