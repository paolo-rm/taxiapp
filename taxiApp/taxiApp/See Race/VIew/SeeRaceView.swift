//
//  SeeRaceView.swift
//  taxiApp
//
//  Created by Paolo Ramos Mendez on 1/16/19.
//  Copyright © 2019 paolo. All rights reserved.
//

import UIKit

class SeeRaceView: UIView {

    let nibName = "SeeRaceView"
    // Our custom view from the XIB file
    @IBOutlet var view: UIView!
    
    @IBOutlet weak var companyImg: UIImageView!
    @IBOutlet weak var driverImg: UIImageView!
    @IBOutlet weak var taxiImg: UIImageView!
    
    @IBOutlet weak var driverInformation: UILabel!
    @IBOutlet weak var carInformation: UILabel!
    
    var car = CarModel(brand: "",
                       model: "",
                       year: "",
                       color: "",
                       plate: "",
                       company: "",
                       city: "",
                       companyImgUrl: "",
                       carImgUrl: "")
    
    var driver = DriverModel(name: "",
                             ci: "",
                             cellPhone: "",
                             driverImgUrl: "")
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.xibSetup()
    }
    
    func xibSetup() {
        self.view = loadViewFromNib()
        self.view.frame = bounds
        self.view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        self.addSubview(self.view)
        self.configureView()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func configureView() {
        
    }
    
    func uploadData(car: CarModel, driver: DriverModel) {
        self.companyImg.downloaded(from: car.companyImgUrl)
        self.taxiImg.downloaded(from: car.carImgUrl)
        self.driverImg.downloaded(from: driver.driverImgUrl)
        
        let driverInfo = "Nombre: \(driver.name)\n" +
            "C.I.: \(driver.ci)\n" +
        "Número de Telefono: \(driver.cellPhone)\n"
        self.driverInformation.text = driverInfo
        
        let carInfo = "Marca: \(car.brand)\n" +
            "Modelo: \(car.model)\n" +
            "Año: \(car.year)\n" +
            "Color: \(car.color)\n" +
            "Placa: \(car.plate)\n" +
            "Empresa: \(car.company)\n" +
            "Ciudad: \(car.city)\n"
        self.carInformation.text = carInfo
    }
    
}
