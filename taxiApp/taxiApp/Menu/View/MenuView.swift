//
//  MenuView.swift
//  taxiApp
//
//  Created by Paolo Ramos Mendez on 12/27/18.
//  Copyright © 2018 paolo. All rights reserved.
//

import UIKit

class MenuView: UIView {

    let nibName = "MenuView"
    // Our custom view from the XIB file
    @IBOutlet var view: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.xibSetup()
    }
    
    func xibSetup() {
        self.view = loadViewFromNib()
        self.view.frame = bounds
        self.view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        self.addSubview(self.view)
        self.configureView()
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func configureView() {
    }
    
    @IBAction func goSettings(_ sender: UIButton) {
        if let vc = self.parentViewController as? MenuViewController {
            vc.goSettings()
        }
    }
    @IBAction func goRace(_ sender: UIButton) {
        Util.showAlertView(message: "Disponible en la siguiente versión.")
//        if let vc = self.parentViewController as? MenuViewController {
//            vc.goRace()
//        }
    }
    @IBAction func seeRace(_ sender: UIButton) {
        if let vc = self.parentViewController as? MenuViewController {
            vc.seeRace()
        }
    }
    @IBAction func goTaxi(_ sender: UIButton) {
        if let vc = self.parentViewController as? MenuViewController {
            vc.goTaxi()
        }
    }
    @IBAction func goQr(_ sender: UIButton) {
        if let vc = self.parentViewController as? MenuViewController {
            vc.goQr()
        }
    }
    
}
