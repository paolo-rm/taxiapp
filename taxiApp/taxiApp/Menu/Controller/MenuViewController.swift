//
//  MenuViewController.swift
//  taxiApp
//
//  Created by Paolo Ramos Mendez on 12/27/18.
//  Copyright © 2018 paolo. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

    var menuView: MenuView!
    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Util.configureTitleView(self, nameXib: "MainTitle")
        self.menuView = MenuView(frame: Util.getContainerFrame(self))
        self.view.addSubview(self.menuView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.topItem?.title = "Viaje Seguro"
    }
    
    func goSettings() {
        let vc = storyBoard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goRace() {
        let vc = storyBoard.instantiateViewController(withIdentifier: "RaceViewController") as! RaceViewController
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func seeRace() {
        let vc = storyBoard.instantiateViewController(withIdentifier: "SeeRaceViewController") as! SeeRaceViewController
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goTaxi() {
        let vc = storyBoard.instantiateViewController(withIdentifier: "TaxiViewController") as! TaxiViewController
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goQr() {
        let vc = storyBoard.instantiateViewController(withIdentifier: "QRCodeViewController") as! QRCodeViewController
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
